//
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit

extension UIViewController {

    func hideStatusBar() {
        UIApplication.shared.isStatusBarHidden = true
    }

    func showStatusBar() {
        UIApplication.shared.isStatusBarHidden = false
    }

    func addActivity() {
        if isActivityOn == true {
            return
        }
        let activity = ActivityView.configure(with: self.view.frame.size)
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.view.addSubview(activity)
            self.view.bringSubviewToFront(activity)
        }, completion: nil)
    }

    private var isActivityOn: Bool {
        for subview in view.subviews {
            if subview is ActivityView {
                return true
            }
        }
        return false
    }

    func removeActivity(animated: Bool) {
        for subview in view.subviews {
            if subview is ActivityView {
                UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    subview.removeFromSuperview()
                }, completion: nil)
            }
        }
    }

    @objc func updateName() {}

    override open func becomeFirstResponder() -> Bool {
        return true
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func addGradient(height: CGFloat) {
        let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height))
        imageView.tag = 777
        imageView.image = #imageLiteral(resourceName: "background")
        view.addSubview(imageView)
        view.sendSubviewToBack(imageView)
    }
    
    func changeGradientHeight(for value: CGFloat) {
        let imageView = view.viewWithTag(777)
        imageView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: value < 64 ? 64 : value)
    }
    
}
