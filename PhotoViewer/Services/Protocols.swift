//
//  Protocols.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/13/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import Foundation

protocol ConvertUrl {
    func returnUrl(from str: String) -> URL?
}

extension ConvertUrl {
    func returnUrl(from str: String) -> URL? {
        return URL(string: str)
    }
}
