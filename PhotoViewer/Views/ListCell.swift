//
//  ListCell.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit

class ListCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
}
