//
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//


import UIKit
import Alamofire

struct Communicator: ConvertUrl {

    typealias block = ((Any) -> Void)?

    private func sendRequest(request: String, method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, headers: Bool = false, completion: @escaping([String: Any]) -> Void) {
        Requester.sendRequest(request: request, method: method, parameters: parameters, headers: headers) { response in
            completion(response)
        }
    }
    
    func getImages(completion: @escaping URLBlock) {
        let request: String = API.GET_MEDIA + Session.accessToken!
        var array: [URL?] = []
        sendRequest(request: request, method: .get) { response in
            let data = response["data"] as? [[String: Any]] ?? []
            _ = data.filter({
                let images = $0["images"] as? [String: Any]
                let standard = images?["standard_resolution"] as? [String: Any]
                let url = standard?["url"] as? String
                array.append(self.returnUrl(from: url ?? "ERROR: no url"))
                return true
            })
            completion(array)
        }
        
    }

}
