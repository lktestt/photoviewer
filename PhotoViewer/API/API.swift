//
//  API.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/4/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit
import Alamofire

typealias ResponseBlock = (([String: Any]) -> Void)
typealias URLBlock = (([URL?]) -> Void)

enum SessionKey: String {
    
    case accessToken = "ACCESS_TOKEN"
    
    static let allValues: [SessionKey] = [accessToken]
}

class Session: NSObject {
    
    private static var sessionKey: String {
        get {
            return (UserDefaults.standard.object(forKey: "key") as? String ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "key")
        }
    }
    
    private static var sessionValue: Any? {
        get {
            return UserDefaults.standard.object(forKey: sessionKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: sessionKey)
        }
    }
    
    private static func sessionValue(get key: SessionKey) -> Any? {
        sessionKey = key.rawValue
        return sessionValue
    }
    
    private static func sessionValue(set value: Any?, key: SessionKey) {
        sessionKey = key.rawValue
        sessionValue = value
    }
    
    static var accessToken: String? {
        get {
            return sessionValue(get: .accessToken) as? String
        }
        set {
            sessionValue(set: newValue, key: .accessToken)
        }
    }
    
}

struct API {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_CLIENT_ID = "48fd992893c74d0a8051da5f8dbdc17f"
    static let INSTAGRAM_CLIENTSERCRET = "c8c2da15980f49d081a1b4aaa569e93c"
    static let INSTAGRAM_REDIRECT_URI = "http://example.com"
    static let INSTAGRAM_ACCESS_TOKEN = "access_token"
    static let INSTAGRAM_SCOPE = "follower_list+public_content"
    static let GET_MEDIA = "https://api.instagram.com/v1/users/self/media/recent/?access_token="
    
}

struct APIConfigs {
    
    static var apiPrefix: String {
        return ""
    }
    
    static var timeoutInterval: Double {
        return 30
    }
    
    static var header: [String: String] {
        return ["Authorization": Session.accessToken ?? ""]
    }
    
    static func request(part: String) -> String {
        return "\(apiPrefix)\(part)"
    }
    
}
