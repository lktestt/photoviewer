//
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

class Requester: NSObject {
    
    static let shared = Requester()
    var sessionManager = Alamofire.SessionManager()
    private var reachabilityManager: NetworkReachabilityManager? = nil
    
    override init() {
        super.init()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIConfigs.timeoutInterval
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private static var manager: Alamofire.SessionManager {
        return shared.sessionManager
    }
    
    class func sendRequest(request: String, method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, headers: Bool = false, completion: @escaping([String: Any]) -> Void) {
        let encoding: ParameterEncoding = method == .get ? URLEncoding.default : JSONEncoding.default
        let checkHeaders = headers ? APIConfigs.header : nil
        manager.request(request, method: method, parameters: parameters, encoding: encoding, headers: checkHeaders).responseJSON(completionHandler: { response in
            if let allHeaderFields = response.response?.allHeaderFields {
                if let token = allHeaderFields["Authorization"] as? String {
                    Session.accessToken = token.components(separatedBy: " ").last
                }
            }
            ResponseValidator.checkResponse(response: response, completion: { response in
                completion(response)
            })
        })
    }
    
}
