//
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//


import UIKit
import Alamofire

class ResponseValidator: NSObject {

    class func checkResponse(response : DataResponse<Any>, completion: @escaping (_ response : [String : Any]) -> Void) {
        if response.result.value == nil {
            manageTimeout(response: response)
            completion([:])
            return
        }
        guard let dictionary = response.result.value as? [String : AnyObject]
            else {
                response.result.value as? [[String : AnyObject]] != nil ? completion(["response" : response.result.value!]) : completion([:])
                return
        }
        _ = gotError(dictionary: dictionary)
        completion(dictionary)
    }
    
    private class func gotError(dictionary : [String : Any]) -> Bool {
        let code = dictionary["errcode"] as? Int ?? 0
        _ = Errors.check(with: code)
        let errorMessage: [String]? = dictionary["errorMsg"] as? [String] ?? dictionary["errmsg"] as? [String]
        if errorMessage == nil { return false }
        return Errors.check(with: code)
    }
    
    private static var excludedStatus : [String] {
        return ["success", "User have not hash", "updated"]
    }
    
    private class func manageTimeout(response : DataResponse<Any>) {
        if let error = response.result.error {
            _ = Errors.check(with: error._code)
            if error._code != -999 || error._code != -1008 {
                print("ERROR")
            }
        }
    }
    
}

class Errors: NSObject {
    
    class func check(with code: Int) -> Bool {
        switch code {
        case 401:
            print("AUTH ERROR")
            return true
        case 4:
            print("json serialization error")
            return true
        case NSURLErrorTimedOut, NSURLErrorCannotFindHost:
            print("ERROR NSURLErrorTimedOut")
            return true
        default: return false
        }
    }
    
}
