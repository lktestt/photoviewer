//
//  ListModel.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/8/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit

class ListModel {
    
    var items: [URL?] = []
    
    func fetch(completion: @escaping () -> Void) {
        UIApplication.topViewController()?.addActivity()
        Communicator().getImages { images in
            self.items = images
            completion()
        }
    }

}
