//
//  ListViewController.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/7/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit
import SDWebImage
import InstagramKit

class ListViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    private lazy var listModel: ListModel = ListModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNIBs()
        getImages()
    }
    
    //MARK: - Helper methods
    
    private func getImages() {
        listModel.fetch {
            self.removeActivity(animated: true)
            self.collectionView.reloadData()
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        addSubviewWithZoomInAnimation(imgView, duration: 1, options: .transitionCrossDissolve, isHidden: true)
        return true
    }
    
    func addSubviewWithZoomInAnimation(_ view: UIView, duration: TimeInterval, options: UIView.AnimationOptions, isHidden: Bool) {
        view.transform = view.transform.scaledBy(x: 0.01, y: 0.01)
        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            self.blackView.isHidden = isHidden
            self.imgView.isHidden = isHidden
            view.transform = CGAffineTransform.identity
        }, completion: nil)
    }

}

extension ListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as? ListCell ?? ListCell()
        cell.imgView?.sd_setImage(with: listModel.items[indexPath.row], completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imgView.sd_setImage(with: listModel.items[indexPath.row], completed: nil)
        addSubviewWithZoomInAnimation(imgView, duration: 1, options: .transitionCrossDissolve, isHidden: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let marginsAndInsets = 20 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + 10 * CGFloat(2)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(3)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func registerNIBs() {
        collectionView.register(UINib(nibName: "ListCell", bundle: nil), forCellWithReuseIdentifier: "listCell")
    }
    
}
