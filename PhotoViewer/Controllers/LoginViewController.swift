//
//  ViewController.swift
//  PhotoViewer
//
//  Created by Lora Kucher on 5/4/19.
//  Copyright © 2019 Lora Kucher. All rights reserved.
//

import UIKit
import WebKit
import InstagramKit

class LoginViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        request()
    }

    private func request() {
        let url = InstagramEngine.shared().authorizationURL(for: InstagramKitLoginScope.publicContent)
        let request = URLRequest.init(url: url)
        webView.load(request)
    }
    
}

extension LoginViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url  {
            InstagramEngine.shared().receivedValidAccessToken(from: url, error: NSErrorPointer.none)
            if let accessToken = InstagramEngine.shared().accessToken {
                Session.accessToken = accessToken
                self.performSegue(withIdentifier: "show", sender: nil)
                webView.stopLoading()
            }
        }
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
}


