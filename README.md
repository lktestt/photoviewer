# PhotoViewer

## Description

PhotoViewer is simple app which uses Instagram API for user's photo view.

## Usage and examples

PhotoViewer has very simple interface. First view is WebKit view which loads Instagram auth viewcontroller. User must auth in this controller for view own photos in next controller.
Next controller is list viewcontroller. In has collectionview with cells where user can see his photos. If user selects some cell, photo will appear in full size with some animation. For closing photo user need swipe up or down on this photo.

Add uses sandbox for working with Instagramm API^ thats why for auth user needs to use test instagram accaunt

LOGIN: accaunt_for
PASS: qwerty12345

```

```
## Refactoring

App has primitive viewmodel for working with API. Refactor this viewmodel with swiftrx.
Refactor Network Services:
- adding more correct pattern State to Response Validator;
- refactor communicator with pattern Bridge;
- create collectionview constructor;
 
## Improvements

Register app in Instagram API Platform(now app uses Sandbox)
In Sandbox app can't use pagination and other usefull methods
Added local db for caching user information. Added observes for db changes that update UI in topviewcontroller.

## Installation

PhotoViewer is available through git clone https://bitbucket.org/lktestt/photoviewer.git
After cloning project use pod install 

```
